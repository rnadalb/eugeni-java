### Hello ; ) ###

Repositori amb el codi de prova per a les classes d'iniciació a la programació orientada a objectes en l'[Institut Eugeni d'Ors](http://www.ies-eugeni.cat/) de [Vilafranca del Penedès](https://goo.gl/maps/XX66YSXFvxB2).

### Exercicis ###

**Operadors**

1. **Esfera**. Calcula la superfície i el volum d'una esfera donat el radi.
1. **Cercle**. Calcula l'àrea d'un cercle donat el radi.
1. **IndexIMC**. Calcula l'índex de masa corporal d'una persona.
1. **Equació**. Mostra el valor de les arrels d'una equació de segon grau. 
1. **TotalHores**. Transforma un interval d'hores en setmanes, dies i hores.

# Llicència #

Copyright (C) 2016 - Rubén Nadal Bellver

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see http://www.gnu.org/licenses/.