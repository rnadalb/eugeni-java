package eugeni.java.basic;

/**
 ** TotalHores: Exemple de construcció d'expressions numèriques.
 ** Transforma un interval d'hores en setmanes, dies i hores.
 */
public class TotalHores {
	public static void main (String [] args ) {
		int totalH, s, d, h;
		totalH = Integer.parseInt(args[0]);
		s = totalH / (24*7);
		d = totalH % (24*7) / 24;
		h = totalH % 24;
		System.out.println("El total de " + totalH + " hores");
		System.out.println("es converteix a:");
		System.out.println(s + " setmanes");
		System.out.println(d + " dies");
		System.out.println(h + " hores");
	}
}