package eugeni.java.basic;

/**
 * IndexIMC: Calcula l'índex de masa corporal d'una persona.
 */
public class IndexIMC {
	public static void main(String[] args) {
		System.out.println("Càlcul de l'índex de masa corporal");
		double peso = 73.4;
		double altura = 1.72;
		double imc = peso / (altura * altura);
		System.out.println("Per un pes de " + peso + " kilograms i");
		System.out.println("una alçada de " + altura + " metres");
		System.out.println("l'índex de masa corporal és de " + (int) imc);
	}
}