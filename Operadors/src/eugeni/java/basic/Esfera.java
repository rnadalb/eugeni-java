package eugeni.java.basic;

/**
 ** Esfera: Exemple de declaració de variables
 ** Calcula la superficie i el volum d'una esfera donat el radi
 */
public class Esfera {
	public static void main (String [] args) {
		double r,s,v;
		final double PI = 3.1416;
		r=5;
		s=4*PI*r*r;
		v=(4/3)*PI*r*r*r;
		System.out.println("Radi de la esfera: " + r + " metres");
		System.out.println("Àrea de la esfera: " + s + " metres quadrats");
		System.out.println("Volum de la esfera: " + v + " metres cúbics");
	}
}