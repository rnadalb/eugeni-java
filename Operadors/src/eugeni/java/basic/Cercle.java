package eugeni.java.basic;

public class Cercle {
	public static void main(String[] args) {
		final double PI = 3.1416;
		double r = Double.parseDouble(args[0]);
		double area;
		
		area = 2*PI*r*r;
		
		System.out.println("El àrea és: " + area);
	}
}
