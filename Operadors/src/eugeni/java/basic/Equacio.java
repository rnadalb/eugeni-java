package eugeni.java.basic;

/**
 ** Equacio: Exemple de construcció d'expressions numèriques.
 ** Mostra el valor de les arrels d'una equació de segon grau.
 */
public class Equacio {
	public static void main(String[] args) {
		double a=1;
		double b=0;
		double c=-1;
		double x1,x2;
		x1 = (-b + Math.sqrt(b*b-4*a*c))/(2*a);
		x2 = (-b - Math.sqrt(b*b-4*a*c))/(2*a);
		System.out.print("Solucions d'una equació de segon grau:");
		System.out.println(a + "x2 + " + b + "x + " + c);
		System.out.println("La primera solució és x1 = " + x1);
		System.out.println("La segona solució és x2 = " + x2);
	}
}
